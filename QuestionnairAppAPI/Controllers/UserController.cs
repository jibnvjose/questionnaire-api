﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuestionnaireApp.Domain.Model;
using QuestionnaireApp.Service.Interface;

namespace QuestionnaireApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService _userService)
        {
            userService = _userService;
        }
        [HttpPost]
        public IActionResult Add(User user)
        {
            var data = userService.Add(user);
            return Ok(data);
        }
    }
}