﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuestionnaireApp.Service.Interface;

namespace QuestionnaireApp.API.Controllers
{
    [Route("api/previleges")]
    [ApiController]
    public class PrevilegeController : ControllerBase
    {
        private readonly IPrevilegeService previlegeService;
        public PrevilegeController(IPrevilegeService _previlegeService)
        {
            previlegeService = _previlegeService;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            var data = previlegeService.GetAll();
            return Ok(data);
        }
    }
}