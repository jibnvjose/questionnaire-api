﻿using QuestionnaireApp.Domain.Model;
using QuestionnaireApp.Repository.Interface;
using QuestionnaireApp.Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Service
{
    public class PrevilegeService:IPrevilegeService
    {
        IPrevilegeRepository previlegeRepository;
        public PrevilegeService(IPrevilegeRepository _previlegeRepository) {
            previlegeRepository = _previlegeRepository;
        }
        public List<Previlege> GetAll()
        {
            var result = previlegeRepository.GetAll();
            return result;
        }
    }
}
