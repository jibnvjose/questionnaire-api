﻿using Microsoft.Extensions.DependencyInjection;
using QuestionnaireApp.Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Service
{
    public static class ServiceRegistration
    {
        public static void AddService(this IServiceCollection services)
        {
            services.AddTransient<IPrevilegeService, PrevilegeService>();
        }
    }
}
