﻿using QuestionnaireApp.Domain.Model;
using QuestionnaireApp.Repository.Interface;
using QuestionnaireApp.Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Service
{
    public class UserService:IUserService
    {
        private readonly IUserRepository userRepository;
        public UserService(IUserRepository _userRepository)
        {
            userRepository = _userRepository;
        }
        public int Add(User user)
        {
            var result = userRepository.Add(user);
            return result;
        }
    }
}
