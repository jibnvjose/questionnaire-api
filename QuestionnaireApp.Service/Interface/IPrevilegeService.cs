﻿using QuestionnaireApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Service.Interface
{
    public interface IPrevilegeService
    {
        public List<Previlege> GetAll();
    }
}
