﻿using QuestionnaireApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Service.Interface
{
    public interface IUserService
    {
        public int Add(User user);
    }
}
