﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class Question
    {
        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public int? Status { get; set; }
        public string QuestionHelpText { get; set; }
    }
}
