﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class InputType
    {
        public int InputTypeId { get; set; }
        public string InputType1 { get; set; }
        public int? Status { get; set; }
    }
}
