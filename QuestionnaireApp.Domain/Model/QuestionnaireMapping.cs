﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class QuestionnaireMapping
    {
        public int QuestionnaireMappingId { get; set; }
        public int? QuestionnaireId { get; set; }
        public int? QuestionId { get; set; }
        public int? AnswerId { get; set; }
        public int? InputTypeId { get; set; }
        public int? OrderNumber { get; set; }
        public double? Score { get; set; }
        public int? Status { get; set; }
    }
}
