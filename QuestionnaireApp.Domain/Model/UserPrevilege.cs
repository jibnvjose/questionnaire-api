﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class UserPrevilege
    {
        public int UserPrevilegeId { get; set; }
        public int? UserId { get; set; }
        public string Previleges { get; set; }
    }
}
