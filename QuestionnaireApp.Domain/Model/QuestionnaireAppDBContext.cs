﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class QuestionnaireAppDBContext : DbContext
    {
        public QuestionnaireAppDBContext()
        {
        }

        public QuestionnaireAppDBContext(DbContextOptions<QuestionnaireAppDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Answer> Answers { get; set; }
        public virtual DbSet<InputType> InputTypes { get; set; }
        public virtual DbSet<Previlege> Previleges { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<Questionnaire> Questionnaires { get; set; }
        public virtual DbSet<QuestionnaireMapping> QuestionnaireMappings { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserAssessment> UserAssessments { get; set; }
        public virtual DbSet<UserPrevilege> UserPrevileges { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=LAPTOP-DPN7308F;Database=QuestionnaireAppDB;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Answer>(entity =>
            {
                entity.ToTable("Answer");

                entity.Property(e => e.AnswerId).HasColumnName("AnswerID");

                entity.Property(e => e.AnswerText)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InputType>(entity =>
            {
                entity.ToTable("InputType");

                entity.Property(e => e.InputTypeId).HasColumnName("InputTypeID");

                entity.Property(e => e.InputType1)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasColumnName("InputType");
            });

            modelBuilder.Entity<Previlege>(entity =>
            {
                entity.ToTable("Previlege");

                entity.Property(e => e.PrevilegeId).HasColumnName("PrevilegeID");

                entity.Property(e => e.Action)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Controller)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DisplayName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.ToTable("Question");

                entity.Property(e => e.QuestionId).HasColumnName("QuestionID");

                entity.Property(e => e.QuestionHelpText)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionText)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Questionnaire>(entity =>
            {
                entity.ToTable("Questionnaire");

                entity.Property(e => e.QuestionnaireId).HasColumnName("QuestionnaireID");

                entity.Property(e => e.QuestionnaireName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QuestionnaireMapping>(entity =>
            {
                entity.ToTable("QuestionnaireMapping");

                entity.Property(e => e.QuestionnaireMappingId).HasColumnName("QuestionnaireMappingID");

                entity.Property(e => e.AnswerId).HasColumnName("AnswerID");

                entity.Property(e => e.InputTypeId).HasColumnName("InputTypeID");

                entity.Property(e => e.QuestionId).HasColumnName("QuestionID");

                entity.Property(e => e.QuestionnaireId).HasColumnName("QuestionnaireID");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.Previleges)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.UserName)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserAssessment>(entity =>
            {
                entity.ToTable("UserAssessment");

                entity.Property(e => e.UserAssessmentId)
                    .ValueGeneratedNever()
                    .HasColumnName("UserAssessmentID");

                entity.Property(e => e.Answer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionId).HasColumnName("QuestionID");

                entity.Property(e => e.QuestionnaireId).HasColumnName("QuestionnaireID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<UserPrevilege>(entity =>
            {
                entity.ToTable("UserPrevilege");

                entity.Property(e => e.UserPrevilegeId).HasColumnName("UserPrevilegeID");

                entity.Property(e => e.Previleges)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
