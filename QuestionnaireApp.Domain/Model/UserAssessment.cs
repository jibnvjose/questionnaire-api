﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class UserAssessment
    {
        public int UserAssessmentId { get; set; }
        public int? UserId { get; set; }
        public int? QuestionId { get; set; }
        public string Answer { get; set; }
        public int? QuestionnaireId { get; set; }
    }
}
