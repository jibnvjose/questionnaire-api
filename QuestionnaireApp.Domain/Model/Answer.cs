﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class Answer
    {
        public int AnswerId { get; set; }
        public string AnswerText { get; set; }
        public int? Status { get; set; }
    }
}
