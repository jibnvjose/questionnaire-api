﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class Questionnaire
    {
        public int QuestionnaireId { get; set; }
        public string QuestionnaireName { get; set; }
        public int? Status { get; set; }
        public int? Duration { get; set; }
    }
}
