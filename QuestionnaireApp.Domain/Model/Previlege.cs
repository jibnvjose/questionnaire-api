﻿using System;
using System.Collections.Generic;

#nullable disable

namespace QuestionnaireApp.Domain.Model
{
    public partial class Previlege
    {
        public int PrevilegeId { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int? Status { get; set; }
        public string DisplayName { get; set; }
        public string GroupName { get; set; }
    }
}
