﻿using Microsoft.Extensions.DependencyInjection;
using QuestionnaireApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Repository
{
    public static class ServiceRegistration
    {
        public static void AddRepository(this IServiceCollection services)
        {
            services.AddTransient<IPrevilegeRepository, PrevilegeRepository>();
        }
    }
}
