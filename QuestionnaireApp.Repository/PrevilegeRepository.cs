﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QuestionnaireApp.Domain.Model;
using QuestionnaireApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuestionnaireApp.Repository
{
    public class PrevilegeRepository: IPrevilegeRepository
    {
        private readonly IConfiguration configuration;
        public PrevilegeRepository(IConfiguration _configuration) {
            configuration = _configuration;
        }
        public int Add(Previlege previlege)
        {
            return 0;
        }
        public List<Previlege> GetAll()
        {
            var sql = "SELECT [PrevilegeID] , [Controller], [Action], [DisplayName], [GroupName], [Status] FROM [Previlege]";
            using (var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = connection.Query<Previlege>(sql).ToList();
                return result;
            }
        }
        public List<Previlege> GetAllByUserID(int userID)
        {
            return null;
        }
    }
}
