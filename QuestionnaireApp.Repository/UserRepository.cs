﻿using Dapper;
using Microsoft.Extensions.Configuration;
using QuestionnaireApp.Domain.Model;
using QuestionnaireApp.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace QuestionnaireApp.Repository
{
    public class UserRepository:IUserRepository
    {
        private readonly IConfiguration configuration;
        public UserRepository(IConfiguration _configuration)
        {
            configuration = _configuration;
        }
        public int Add(User user)
        {
            var sql = "INSERT INTO USER(UserName, Password, FirstName, LastName, MiddleName, Status) VALUES(@UserName, @Password, @FirstName, @LastName, @MiddleName, @Status)";
            using(var connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = connection.Execute(sql, user);
                return result;
            }
        }
    }
}
