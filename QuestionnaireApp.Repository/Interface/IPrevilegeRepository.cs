﻿using QuestionnaireApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Repository.Interface
{
    public interface IPrevilegeRepository
    {
        public int Add(Previlege previlege);
        public List<Previlege> GetAll();
        public List<Previlege> GetAllByUserID(int userID);

    }
}
