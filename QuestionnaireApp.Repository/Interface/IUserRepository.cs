﻿using QuestionnaireApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Repository.Interface
{
    public interface IUserRepository
    {
        public int Add(User user);

    }
}
