﻿using QuestionnaireApp.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionnaireApp.Repository.Interface
{
    public interface IUserPrevilegeRepository
    {
        public int Add(UserPrevilege userPrevilege);
    }
}
